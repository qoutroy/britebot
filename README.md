# Brit Bot
A discord bot.

# Install
## Install node and npm
### Raspberry Pi

```
wget https://nodejs.org/dist/v8.9.0/node-v8.9.0-linux-armv6l.tar.gz
```

```
tar -xzf node-v8.9.0-linux-armv6l.tar.gz
```

```
cd node-v6.11.1-linux-armv6l/ && sudo cp -R * /usr/local/
```

To check if node and npmm got installed:
`node -v` and `npm -v` should both print their version.
### Other OS's
https://nodejs.org/en/

## Install The Bot
Go to the directory:
```
cd /path/to/discordbot
```
Install packages:
```
npm install
```
Now change this line:
```
token: 'YOUR TOKEN',
```
To whatever your bot token is. You are now ready to run the bot!

# Run the bot
```
npm run start
```

You might get an error. This might say something about FFMPG. Then install it [here](https://www.ffmpeg.org/) and try again.([Install FFMPEG on raspberry pi](https://wiki.debian.org/ffmpeg))