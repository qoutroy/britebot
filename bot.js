const Discord = require('discord.js');
const auth = require('./auth');
var bot = new Discord.Client();

bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}!`);
});

let isReady = true;
bot.on('message', msg => {
    if(isReady && isCommand(msg)) {
        isReady = false;
        let vc = msg.member.voiceChannel;
        
        if(vc)
            vc.join().then(con => {
                const dispatcher = con.playFile('./sounds/snipe.wav');
                dispatcher.on("end", end => {
                   vc.leave();
                   msg.channel.send('Bot by Qoutroy');
                });
                isReady = true;
            })
            .catch(console.err);
        else  msg.channel.send('Please, join a channel!');
    }
});

function isCommand(str){
    return str == auth.command
}

bot.login(auth.token);
